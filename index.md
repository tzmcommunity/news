---
layout: redirect
bootstrap: true
redirect_to: https://tzm.community

# title
postnav_title: "Zeitgeist Movement - Development and news"

# second title
postnav_subtitle: "From community to community"

# second link
postnav_link: "abouttzm"

# second linktext
postnav_linktext: "Learn more"

# home page header image
header_image: "/assets/img/autumn-219972_1280.jpg"

---

<h2>What is this?</h2>

Posts here are created through #development_and_news channel on TZM Discord server. Invite link here: https://tzm.community/discord

